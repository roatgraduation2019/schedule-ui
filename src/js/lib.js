const modifyLessons = (obj) => {
    for (let profCount = 0; profCount < obj.length; profCount++) {
        for (let datesCountOfProf = 0; datesCountOfProf < obj[profCount].dates.length; datesCountOfProf++) {
            let lessonsShort = obj[profCount].dates[datesCountOfProf].lessons;
            let lastLessonCount = lessonsShort.length - 1;
            if (lessonsShort[lastLessonCount]) {
                if (lessonsShort[lastLessonCount].start_time == "9.00") {
                    lessonsShort.unshift(obj[profCount].dates[datesCountOfProf].lessons[lastLessonCount]);
                    lessonsShort.pop();
                }
            }

            let absoluteLessonsNumber = 0;
            let lessonsNumberFromArray = 0;
            let modifiedLessons = [];
            let emptyLesson = { "isEmpty": true };
            while (absoluteLessonsNumber < 7) {
                if (lessonsShort[lessonsNumberFromArray] && lessonsShort[lessonsNumberFromArray].start_time == lessonsTime[absoluteLessonsNumber].startTime) {
                    modifiedLessons.push(lessonsShort[lessonsNumberFromArray]);
                    absoluteLessonsNumber++;
                    lessonsNumberFromArray++;
                }
                else {
                    modifiedLessons.push(emptyLesson);
                    absoluteLessonsNumber++;
                }
            }
        obj[profCount].dates[datesCountOfProf].lessons = modifiedLessons;
        }
    }
};

const getLessonsByDate = (profObjectDates, date) => {
    for (let dateCount = 0; dateCount < profObjectDates.length; dateCount++) {
        if (profObjectDates[dateCount].date == date) {
            return profObjectDates[dateCount].lessons
        }
    }
}

const getLessonsByProfessorName = (obj, proffName) => {
    for (let profCount = 0; profCount < obj.length; profCount++) {
        if (obj[profCount].professor_name ==  proffName){
            return obj[profCount].dates;
        }
    }
}

const getLessonRow = (lessonsArray) => {
    let lessonRow = ``;
    for (let i = 0; i < lessonsTime.length; i++) {
        const lesson = lessonsArray[i];
        if (lessonsArray[i].isEmpty) {
            lessonRow += `<td></td><td></td>`;
        }
        else {
            lessonRow += `<td>${lesson.group}<br>чел.: ${lesson.stud_count}</td><td>${lesson.auditory}<br>мест: ??</td>`;
        }
    }
    return lessonRow
};

const renderSchedule = (string) => {

    if ($('.current-row') != undefined) {
        $('.current-row').remove();
    }

    let currentDate = $('.date-form').val();
    currentDate = currentDate.slice(8) + '.' + currentDate.slice(5,7) + '.' + currentDate.slice(0,4);

    for (let profCount = 0; profCount < string.length; profCount++) {
        const LessonsByDate = getLessonsByDate(string[profCount].dates, currentDate);
        if (LessonsByDate) {
            let newTableRow = `<td>${string[profCount].professor_name}</td>` + getLessonRow(LessonsByDate);
            $('<tr>').html(newTableRow)
                .addClass('current-row')
                .appendTo('tbody');
        }
    }
};
