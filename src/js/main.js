$('.date-apply-button').click(function (event) {
    event.preventDefault();

    $.ajax({
        url: "data.json",
        success: function(data) {
            modifyLessons(data);
            renderSchedule(data);
        }
    });
});
