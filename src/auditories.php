<html>
<head>
	<meta charset= "utf-8">
	<title>Аудитории</title>
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<style type="text\css">
</style>
</head>

<body>

<div class="col-6 col-md-3">
	<button type="button" class="btn btn-outline-secondary">Добавить аудиторию</button>
	<a href="index.php" class=" btn btn-outline-secondary" role="button" aria-pressed="true">Вернуться</a>
</div>

<div class="col-6 col-md-3">
	
 <table border=2 class="table table-striped" style="font-size: 15px; text-align: center">
	   
   <tr>	
      <th>Номер аудитории</th>
      <th>Количество мест</th>
	  <th></th>
   </tr>
		  
   <tr>	
      <td>
		<div class="input-group">
			<input type="text" class="form-control" aria-label="Text input with segmented dropdown button">
				<div class="input-group-append">
					<button  class="edit-ok, btn btn-outline-secondary"><i class="far fa-check-circle"></i></button>
					<button type="button" class="edit-cancel, btn btn-outline-secondary"><i class="fas fa-times-circle"></i></button>
				</div>
		</div>
	  </td>
      <td>20</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>  
   </tr>
		  
   <tr>	 
      <td>314</td>
      <td>40</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	 
      <td>313</td>
      <td>43</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>
	 <td>106</td>
     <td>50</td>
	 <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	
      <td>307</td>
      <td>20</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	 
      <td>106</td>
      <td>50</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	
      <td>108</td>
      <td>51</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	 
      <td>127</td>
      <td>945</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		
   <tr>	 
      <td>490</td>
      <td>100</td>
	  <td>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Delete">
		  <span aria-hidden="true">&times;</span>
		</button> 
	  </td>
   </tr>
		  
 </table>
</div>


</body>
</html>