<?php
require_once 'config.php';
session_start();

/**
 * Проверка на загруженность данных из файла
 * @return boolean [description]
 */
function has_data()
{
    return !empty($_SESSION['schedule_data']);
}

/**
 * Получение данных из файла
 * @return
 */
function get_data()
{
    if (has_data()) {
        return $_SESSION['schedule_data'];
    }
    return false;
}

/**
 * Отрисовка шапки таблицы расписания
 * @param $date
 */
function render_header($date)
{
    global $lesson_times;
    setlocale(LC_ALL, 'ru_RU.utf8');
    $lesson_count = count($lesson_times);
    $column_count = 1 + 2 * $lesson_count;
    $formatted_date = strftime('%A %d.%m.%Y', $date);
    $formatted_date = mb_convert_case($formatted_date, MB_CASE_TITLE);
?>
    <thead>
        <tr>
            <th colspan="<?php echo $column_count; ?>">
                Расписание занятий
            </th>
        </tr>
        <tr>
            <th rowspan="2"></th>
            <th colspan="<?php echo $column_count - 1; ?>">
                <?php echo $formatted_date; ?>
            </th>
        </tr>
        <tr>
    <?php
        foreach($lesson_times as $time) {
            echo "<th colspan='2'>{$time[0]} - {$time[1]}</th>";
        }
    ?>
        </tr>
        <tr>
            <th>Преподаватель</th>
    <?php
        foreach($lesson_times as $time) {
            echo "<th >Группа</th>";
            echo "<th >Аудит.</th>";
        }
    ?>
        </tr>
    </thead>
<?php
}

function render_professor_line($data)
{
    foreach(array_keys($data) as $professor){
        echo "<tr>";
        echo "<td>{$professor}</td>";
        render_lesson_cells($data[$professor]);
        echo "</tr>";
    }
}

function render_lesson_cells($lessons)
{
    global $lesson_times;
    foreach ($lessons as $value) {
        $array_var[] = $value['start_time'];
        //обьединяет время начала пар в массив для соответствия
    }
    $return_str = "";
    for($i = 0, $a = 0; $i < count($lesson_times); $i++) {
        if (in_array($lesson_times[$i][0], $array_var)){
            if($lessons[$a]['auditory']==''){
                $auditory = '<span class="text-danger">Нет информации об аудитории</span>';
            } else {
                $auditory = $lessons[$a]['auditory'];
            }
            $return_str .= '<td>'.$lessons[$a]['group']
                . '</br>чел.: '
                . $lessons[$a]['stud_count']
                . '</td><td>'
                . $auditory
                . '</td>';
            $a++;
        } else {
            $return_str .= '<td></td><td></td>';
        }
    }
    echo $return_str;
    $array_var = [];
}

function render_data($selected_date)
{
    echo "<tbody>";
    $data = get_data();
    if ($data === false) {
        echo "<tr><td colspan='15'>Данные отсутвуют</td></tr>";
    } else {
        render_professor_line($data[$selected_date]);
    }
    echo "</tbody>";
}

function result_for_php_word($selected_date,$table){
	//$selected_date = '2018-10-04';
	$selected_date = substr($selected_date,8,2).'.'.substr($selected_date,5,2).'.'.substr($selected_date,0,4);
	$json_file = file_get_contents('data.json');
	$data = json_decode($json_file, true);

	$time_table = ['09.00','10.30','12.00','14.10','15.40','17.10','18.40'];
	$return_str = '';

	foreach($data as $prof){
		foreach($prof['dates'] as $date){
			if ($date['date']==$selected_date){
				$row = $table->addRow();
				$row -> addCell(1000)->addText($prof['professor_name']);
				foreach ($date['lessons'] as $value) {
					$array_var[] = $value['start_time'];
					//обьединяет время начала пар в массив для соответствия
				}

				for($i=0,$a=0;$i<15;$i++){
					if (in_array($time_table[$i], $array_var)){
						$row->addCell(1000)->addText($date['lessons'][$a]['group']);
						$row->addCell(1000)->addText($date['lessons'][$a]['auditory']);
						$a++;

					}else{
						$row->addCell(1000)->addText('');
					}
				}
			$array_var = [];

			}
		}
	}
}

?>
