<?php
require_once 'config.php';


function array_search_by($array, $callback)
{
    foreach($array as $key=>$value) {
        if ($callback($value, $key)) {
            return $key;
        }
    }
    return false;
}


function format_data($data)
{
    if (is_string($data)) {
        $data = json_decode($data, true);
    }
    foreach($data as &$date){
        foreach ($date as $proffessor => $lessons) {
            $date[$proffessor] = get_formatted_lessons($lessons);
        }
    }
    return $data;
}

function get_formatted_lessons($lessons)
{
    $formatted_lessons = array();
    foreach($lessons as $lesson) {
        $lessons_count = get_lesson_pairs(
                $lesson['start_time'],
                $lesson['end_time']
            );
        foreach ($lessons_count as $time) {
            $detailed_lesson = $lesson;
            $detailed_lesson['start_time'] = $time[0];
            $detailed_lesson['end_time'] = $time[1];
            array_push($formatted_lessons, $detailed_lesson);
        }
    }
    return $formatted_lessons;
}

function get_lesson_pairs($start, $end)
{
    global $lesson_times;
    $pairs = array();
    $find_start = function ($lesson) use ($start) {
        return $lesson[0] === $start;
    };
    $find_end = function ($lesson) use ($end) {
        return $lesson[1] === $end;
    };
    $start_index = array_search_by($lesson_times, $find_start);
    $end_index = array_search_by($lesson_times, $find_end);
    $length = $end_index - $start_index + 1;
    return array_slice($lesson_times, $start_index, $length);
}