<?php
require_once 'result.php';

$wdays = array(
        'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
        'Четверг', 'Пятница', 'Суббота'
    );

$show_date = empty($_GET['date']) ? "" : $_GET['date'];
$export_start_date = empty($_POST['start_date']) ? "" : $_POST['start_date'];
$export_end_date = empty($_POST['end_date']) ? "" : $_POST['end_date'];

?><!DOCTYPE html>
<html>
<head>
    <meta charset= "utf-8">
    <title>Расписание ЖАТС</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <style type="text/css">
        .custom-file-label::after {
            content: "Обзор";
        }
        .schedule-table {
            font-family: 'Times New Roman', serif;
            font-size: 12pt;
            text-align: center;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <a class="navbar-brand" href="/">Расписание ЖАТС</a>
    <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="auditories.php">
                    Аудитории
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="w-auto p-3 bg-light">
    <form
        method="POST"
        enctype="multipart/form-data"
        action="uploadfile.php">
        <div class="form-row">
            <div class="custom-file col">
                <input
                    type="file"
                    class="custom-file-input"
                    name="myFile"
                    id="customFile">
                <label
                    class="custom-file-label"
                    for="customFile">
                    Выбрерите файл
                </label>
            </div>
            <div class="col">
                <button
                    type="submit"
                    class="btn btn-secondary">
                    Загрузить данные
                </button>
            </div>
        </div>
        <a href="/data/test.xlsx"
            class="badge badge-secondary"
            target="_blank">
            Пример файла с данными
        </a>
    </form>
</div>

<?php
    if (has_data()) {
        $data = get_data();
        $dates = array_keys($data);
        $compare_dates = function($a, $b) {
            return strtotime($a) - strtotime($b);
        };
        usort($dates, $compare_dates);
?>
<div class="w-auto p-3 bg-light">
    <div class="row">
<?php
        foreach ($dates as $date_text) {
            $button_cls = $date_text === $show_date ? 'btn-dark' : 'btn-info';
?>
        <div class="col-md-2 col-sm-3 col-4 mb-2">
            <form method='GET'>
                <input type="hidden" name="date" value="<?php echo $date_text; ?>">
                <input
                    class="btn btn-sm btn-block <?php echo $button_cls; ?>"
                    type="submit"
                    <?php echo $date_text === $show_date ? 'disabled' : ''; ?>
                    value="<?php echo $date_text; ?>">
            </form>
        </div>
<?php
        }
?>
    </div>
</div>
<?php
    }
?>

<!-- TODO скрываем, пока не будет полноценно реализовано
<div  class="w-auto p-3" style="background-color: #eee;">
     <form class="form-inline" method = 'post' action="phpword.php">
        <div class="form-group">
            <label for="colFormLabel" class="my-1 mr-2">
                Выберите даты для экспорта:
            </label>
            <input
                type="date"
                name='start_date'
                class="form-control date-form"
                id="colFormLabel"
                value="<?php echo $export_start_date; ?>">
            <input
                type="date"
                name='end_date'
                class="form-control date-form"
                id="colToLabel"
                value="<?php echo $export_end_date; ?>">
            <button
                type = "submit"
                class="date-apply-button btn btn-outline-secondary">
                Скачать
            </button>
        </div>
    </form>
</div>
-->

<div class="w-auto p-3" style="overflow:auto;">
<?php
    if (!has_data()) {
?>
    <div class="alert alert-secondary text-center" role="alert">
        Загрузите файл с данными из АСПК РОАТ
    </div>
<?php
    } else if (!empty($show_date)) {
?>
    <table
        border=4
        class="schedule-table table table-striped">
    <?php
        render_header(strtotime($show_date));
        render_data($show_date);
    ?>
    </table>
<?php
    } else {
?>
    <div class="alert alert-secondary text-center" role="alert">
        Выберите дату отображения
    </div>
<?php
    }
?>
</div>

<div
    class="modal fade"
    id="lessonEditor"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Редактирование занятия
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                <form>
                    <div class="form-group row">
                        <label
                            for="nameLesson"
                            class="col-sm-5 col-form-label">
                            Название дисциплины:
                        </label>
                        <div class="col-sm-7">
                            <input
                                type="text"
                                class="form-control"
                                id="nameLesson">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            for="countStudents"
                            class="col-sm-5 col-form-label">
                            Количество студентов:
                        </label>
                        <div class="col-sm-7">
                            <input
                                type="integer"
                                class="form-control"
                                id="countStudents">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            for="colFormLabel"
                            class="col-sm-5 col-form-label">
                            Номер аудитории:
                        </label>
                        <div class="col-sm-7">
                            <input
                                type="integer"
                                class="form-control"
                                id="auditoryNumber">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label
                            for="colFormLabel"
                            class="col-sm-5 col-form-label">
                            Количество мест:
                        </label>
                        <div class="col-sm-7">
                            <input
                                type="integer"
                                class="form-control"
                                id="colFormLabel">
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-outline-secondary"
                    data-dismiss="modal">
                    Отмена
                </button>
                <button
                    type="button"
                    class="btn btn-outline-secondary">
                    Сохранить
                </button>
            </div>
        </div>
    </div>
</div>

    <script src="/js/lib/jquery-3.3.1.min.js"></script>
    <script src="/js/lib/popper.min.js"></script>
    <script src="/js/lib/bootstrap.min.js"></script>
    <script src="/js/lib/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            bsCustomFileInput.init();
        });
    </script>
</body>
</html>