<?php
session_start();
require_once 'reformatter.php';

$parserHost = getenv('SCHEDULE_PARSER_HOST');
if ( empty($parserHost) ) {
    $parserHost = 'http://localhost:5000';
}

$cfile = new CURLFile($_FILES['myFile']['tmp_name']);
$data = array('file' => $cfile);

$ch = curl_init("$parserHost/json");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$output = curl_exec($ch);
$_SESSION["schedule_data"] = format_data($output);

header("Location: index.php");
