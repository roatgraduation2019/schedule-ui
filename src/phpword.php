<?php
header('Content-Type: text/html; charset=utf-8');
require 'result.php';
require 'vendor/autoload.php';
$gr_aud=['гр.','ауд.'];

$array_time = array(
"start_time_norm" => ['09','10','12','14','15','17','18'],
"start_time_up" => ['00','30','00','10','40','10','40'],
"end_time_norm" => ['-10','-11','-13','-15','-17','-18','-20'],
"end_time_up" => ['20','50','20','30','00','30','00']
);

// $start_date = "2018-10-01";
// $end_date = "2018-10-05";
// $current_date = "2018-10-03";

$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$current_date = $_POST['start_date'];




$start_date_title = date('d.m.Y', strtotime($start_date));
$end_date_title = date('d.m.Y', strtotime($end_date));

$wdays = array(
        'Воскресенье', 'Понедельник', 'Вторник', 'Среда',
        'Четверг', 'Пятница', 'Суббота'
    );


//echo $start_date.'<br>'.$end_date;




$phpWord = new  \PhpOffice\PhpWord\PhpWord();

$phpWord->setDefaultFontName('Times New Roman');
$phpWord->setDefaultFontSize(12);

$properties = $phpWord->getDocInfo();

$properties->setCreator('My name');
$properties->setCompany('My factory');
$properties->setTitle('My title');
$properties->setDescription('My description');
$properties->setCategory('My category');
$properties->setLastModifiedBy('My name');
$properties->setCreated(mktime(0, 0, 0, 3, 12, 2014));
$properties->setModified(mktime(0, 0, 0, 3, 14, 2014));
$properties->setSubject('My subject');
$properties->setKeywords('my, key, word');


$tableStyle = array(
    'borderColor' => '006699',
    'borderSize'  => 6,
    'cellMargin'  => 10

);


$sectionStyle = array('orientation' => 'portrait',
               'marginLeft' => 350, //Левое поле равно 15 мм
               'marginRight' => 350,
               'marginTop' => 350,
               'borderTopColor' => 'C0C0C0'
         );




//$header = array('size' => 16, 'bold' => true);
// 1. Basic table
// $rows = 10;
// $cols = 5;

/*
 *  4. colspan (gridSpan) and rowspan (vMerge)
 *  ---------------------
 *  |     |   B    |  1 |
 *  |  A  |        |----|
 *  |     |        |  2 |
 *  |     |---|----|----|
 *  |     | C |  D |  3 |
 *  ---------------------
 * @see https://github.com/PHPOffice/PHPWord/issues/806
 */

$time_style_up = array('superScript'=>true);
//$time_main_num = [];
$noSpace = array('spaceAfter' => 0);

//часть заголовка таблицы, не меняется, кроме даты

$days = substr($end_date_title, 0,2)-substr($start_date_title, 0,2);
$current_date_title = date('d.m.Y', strtotime("0 day", strtotime($start_date)));
$current_day = $wdays[(date('w',strtotime("0 day", strtotime($start_date))))];
$day_count =0;

while($day_count<=$days){

$section = $phpWord->addSection($sectionStyle);
$row_style = array('exactHeight' => 567,'cantSplit' => true);

$styleTable = array('borderSize' => 6, 'borderColor' => '999999','align'=>'center');
$phpWord->addTableStyle('Colspan Rowspan', $styleTable);
$table = $section->addTable('Colspan Rowspan');
$row = $table->addRow();
    $row->addCell(1700, array('vMerge' => 'restart'))->addText('ФИО Преподователя',$center_text);//1-ая ячейка для ФИО преподователя
    $row->addCell(9213, array('gridSpan' => 14, 'vMerge' => 'restart'))->addText("Расписание занятий с $start_date_title по $end_date_title");
$row = $table->addRow();
     $row->addCell(1700, array('vMerge' => 'continue'));//2-ая ячейка для ФИО преподователя
     $row->addCell(9213, array('gridSpan' => 14, 'vMerge' => 'restart'))->addText("$current_day $current_date_title");
$row = $table->addRow();
    $row->addCell(1700, array('vMerge' => 'continue'));	//3-ая ячейка для ФИО преподователя
for ($i=0; $i<7;$i++){
	$r = $row->addCell(1270, array('gridSpan' => 2, 'vMerge' => 'restart'));
	$textrun = $r->createTextRun(); 
	$textrun->addText($array_time["start_time_norm"][$i]);
	$textrun->addText($array_time["start_time_up"][$i],$time_style_up);
    $textrun->addText($array_time["end_time_norm"][$i]);
    $textrun->addText($array_time["end_time_up"][$i],$time_style_up);
}
$row = $table->addRow();
$row->addCell(1700, array('vMerge' => 'continue'));//4-ая ячейка для ФИО преподователя

for ($i=0; $i<14;$i++){
  if ($i % 2==0){
    $r = $row->addCell(817)->addText($gr_aud[0],array('size'=>10));
  }else{
    $r = $row->addCell(453)->addText($gr_aud[1],array('size'=>10));
  }
	
}

//result_for_php_word($current_date_title,$table);

  //$selected_date = substr($selected_date,8,2).'.'.substr($selected_date,5,2).'.'.substr($selected_date,0,4);
  //$selected_date =$current_day;
  $json_file = file_get_contents('data.json');
  $data = json_decode($json_file, true);

  $time_table = ['09.00','10.30','12.00','14.10','15.40','17.10','18.40'];
  $return_str = '';

  foreach($data as $prof){
    foreach($prof['dates'] as $date){
      if ($date['date']==$current_date_title){
        $row = $table->addRow($row_style);
        $professor_name = explode(' ', $prof['professor_name']);
        $professor_name[1] = substr($professor_name[1], 0, 2);
        $professor_name[2] = substr($professor_name[2], 0, 2);
        $professor_name = "$professor_name[0] $professor_name[1]. $professor_name[2].";
        $row -> addCell(1700)->addText($professor_name);
        foreach ($date['lessons'] as $value) {
          $array_var[] = $value['start_time'];
          //обьединяет время начала пар в массив для соответствия
        }
        
        for($i=0,$a=0;$i<7;$i++){
          if (in_array($time_table[$i], $array_var)){
            if($date['lessons'][$a]['auditory']=='nan'){
              $auditory = 'Нету информации о аудитории';
              $aud_style_error = array('size'=>8,'color' => 'FF0000');
            }else {
              $auditory = $date['lessons'][$a]['auditory'];
              $aud_style_error = array('size'=>8);
            }
            $row->addCell(817)->addText($date['lessons'][$a]['group'],array('size'=>8));
            $row->addCell(453)->addText($auditory,$aud_style_error);
            $a++;
            
          }else{
            $row->addCell(817)->addText('');
            $row->addCell(453)->addText('');
          }
        }
      $array_var = [];  
      
      }
    }
  }
$section->addPageBreak();
$day_count++;
$current_date_title = date('d.m.Y', strtotime("+ $day_count day", strtotime($start_date)));
$current_day = $wdays[(date('w',strtotime("+ $day_count day", strtotime($start_date))))];
}







$fontStyle = array('name' => 'Times New Roman', 'size' => 16,'color' => '075776','italic'=>true);
$listStyle = array('listType'=>\PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER);


$fontStyle = array('name' => 'Times New Roman', 'size' => 48,'color' => '075776','italic'=>true);
$phpWord->addTitleStyle(6,$fontStyle);
//$section->addTitle('Заголовок',6);      						


//$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
//$objWriter->save('doc.docx');	

header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=$start_date-$end_date.docx");
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$xmlWriter->save("php://output");	


?>