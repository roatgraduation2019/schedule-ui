FROM roquie/docker-php-webapp:7.3-latest
USER root
RUN apt-get update \
    && apt-get install -y locales \
    && rm -rf /var/lib/apt/lists/* \
    && localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8
ENV LANG ru_RU.utf8
ENV SCHEDULE_PARSER_HOST https://schedule-parser.semochkin.now.sh
COPY --chown=nginx:nginx src/ /srv/www
# RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# RUN composer install
