# Сервис пользовательского интерфейса проекта "Расписание учебных занятий"

## Запуск приложения на локальной машине

1. На локальной машине должно быть установлено `php-cli` и `composer`
2. Установка php-зависимостей:

    ```
    cd src
    composer install
    ```

3. Запус локального сервера:

    ```
    php -t src/ -S localhost:8000
    ```


## Запуск приложения через Docker

1. Создать образ: 

    ```
    docker build -t schedule-main .
    ```

2. Запустить контейнер образа `schedule-main` и "пробросить" порт 8080 на хостовой машине или указать его в качестве переменной окружения:

    ```
    docker run -p 8000:8080 schedule-main
    docker run -p 8000:5000 --env PORT=5000 schedule-main
    ```

## Установка приложения на стенд Heroku

1. На локальной машине должно быть установлено `heroku-cli` и `docker`
2. Авторизоваться в Heroku:

    ```
    heroku login
    ```

3. Авторизоваться в Heroku Containers:

    ```
    heroku container:login
    ```

4. Собрать docker-образ и запушить его в Heroku Containers:

    ```
    heroku container:push -a <app_name> web
    ```

5. Развернуть контейнер из отправленного образа на стенде:

    ```
    heroku container:release -a <app_name> web
    ```